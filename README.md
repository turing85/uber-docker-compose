# uber-docker-compose
A (n opinionated) collection of useful docker-images for development, and some 
sample configurations.

The [`docker-compose.yml`](./localdeployment/docker-compose.yml) includes some
docker-images plus some pre-configuration one might find useful when developing 
applications.

If a container needs configuration through an external file, the file can be 
found in the directory with the same name as the container. For example, 
container `artemis` uses a volume mount for the file located in directory 
[`./localdeployment/artemis`](./localdeployment/artemis). Within the respective 
directory, the directory structure of the mount path in the container is 
replicated. Thus, directory 
[`./localdeployment/artemis/var/lib/artemis/etc-override`](localdeployment/artemis/var/lib/artemis/etc-override)
is mounted into the container `artemis` under the path `/var/lib/artemis/etc-override`. Whether
a single file or a whole directory is mounted depends on the container and its
directory layout. In general, a whole directory mount should be preferred.

Application stacks (for example kafka + zookeeper + web UIs) are grouped 
together and separated from other services by a blank line.