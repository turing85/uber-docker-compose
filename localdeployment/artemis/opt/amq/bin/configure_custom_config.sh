#!/usr/bin/env bash
set -e

INSTANCE_DIR=${1}

echo ${INSTANCE_DIR}
cp /opt/amq/bin/broker.xml ${INSTANCE_DIR}/etc/broker.xml