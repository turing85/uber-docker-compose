#!/usr/bin/env bash
if [ -n '$MONGO_INITDB_DATABASE' ]; then
  echo "
  db.createUser({user: '$MONGO_INITDB_DATABASE', pwd: '$MONGO_INITDB_DATABASE', roles: [{ role: 'root', db: 'admin' }] })" | mongosh \
    $MONGO_INITDB_DATABASE \
    --username $MONGO_INITDB_ROOT_USERNAME \
    --password $MONGO_INITDB_ROOT_PASSWORD \
    --authenticationDatabase admin
fi